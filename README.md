Nginx and PHP for Docker

## Installation
Pull the image from the docker index rather than downloading the git repo. This prevents you having to build the image on every docker host.
```sh
docker pull aamirmunirdev/php70
```

## Running
To simply run the container:
```sh
docker run --name nginx -p 8080:80 -d aamirmunirdev/php70
```
You can then browse to http://\<docker_host\>:8080 to view the default install files.

## Volumes
If you want to link to your web site directory on the docker host to the container run:
```sh
docker run --name nginx -p 8080:80 -v /your_code_directory:/data/www -d aamirmunirdev/php70
```